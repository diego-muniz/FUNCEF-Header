(function () {
    'use strict';
    /**
    * @ngdoc overview
    * @name Validate
    * @version 1.0.0
    * @Componente para validação de formulários
    */
    angular.module('funcef-header.controller', []);
    angular.module('funcef-header.directive', []);

    angular
    .module('funcef-header', [
      'funcef-header.controller',
      'funcef-header.directive',
      'ngCookies'
    ]);
})();;(function () {
    'use strict';

    angular.module('funcef-header.controller').
        controller('HeaderController', HeaderController);

    HeaderController.$inject = ['$rootScope', '$cookies', '$state', '$timeout', '$scope'];

    /* @ngInject */
    function HeaderController($rootScope, $cookies, $state, $timeout, $scope) {

        var vm = this;
        vm.toggleMenu = toggleMenu;
        vm.logout = logout;
        vm.trocarUsuario = trocarUsuario;

        /////////////

        /* Fecha e abre menu*/
        function toggleMenu() {
            $rootScope.menuOpen = !$rootScope.menuOpen;
            $cookies.put('menuOpen', $rootScope.menuOpen);
        }

        function logout() {
            $rootScope.usuario = null;

            $timeout(function () {
                $state.go('logout');
            }, 500);

        }

        function trocarUsuario() {
            $rootScope.usuario = null;

            $timeout(function () {
                $state.go('trocar-usuario');
            }, 500);

        }
    }
})();;(function () {
    'use strict';

    angular
      .module('funcef-header.directive')
      .directive('ngfHeader', ngfHeader);

    /* @ngInject */
    function ngfHeader() {
        return {
            restrict: 'EA',
            replace: true,
            transclude: true,
            templateUrl: 'views/header.view.html',
            controller: 'HeaderController',
            controllerAs: 'vm',
            scope: {
                logo: '@',
                sistema: '@',
                text: '='
            }
        };
    }
})();;angular.module('funcef-header').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('views/header.view.html',
    "<header> <nav class=\"navbar navbar-inverse navbar-fixed-top\"> <div class=\"navbar-header\"> <a ui-sref=\"home\" class=\"navbar-brand text-shadow-white\"> <img ng-src=\"{{logo}}\" alt=\"{{sistema}}\" width=\"auto\" height=\"35\" class=\"text-left\" ng-if=\"logo\"> <span class=\"text-left\" ng-if=\"text === undefined || text === true \">{{sistema}}</span> </a> </div> <div class=\"navbar-collapse collapse\" ng-if=\"$root.usuario\"> <ul class=\"nav navbar-nav pull-right mr-10\"> <li class=\"dropdown\"> <a class=\"dropdown-toggle profile-image\" data-toggle=\"dropdown\"> <span ng-if=\"!$root.usuario.Foto\" class=\"letter-user\"> {{$root.usuario.Nome.charAt(0)}} </span> <div ng-if=\"$root.usuario.Foto\" class=\"img-circle\"> <img ng-if=\"$root.usuario.Foto\" ng-attr-src=\"{{$root.usuario.Foto}}\"> </div> <span>{{$root.usuario.Nome | nomeUsuario}}</span> </a> <ul class=\"dropdown-menu dropdown-menu-right\"> <li> <a ng-click=\"vm.trocarUsuario()\"> <i class=\"fa fa-user\"></i> Trocar usuário </a> </li> <li role=\"separator\" class=\"divider\"></li> <li> <a ng-click=\"vm.logout()\"> <i class=\"fa fa-sign-out\"></i> Sair </a> </li> </ul> </li> </ul> <ul class=\"nav navbar-nav\"> <li> <a href=\"javascript:void(0);\" ng-click=\"vm.toggleMenu()\" ng-show=\"$root.usuario.Menu.length\" class=\"toggle-left-sidebar\"> <i class=\"glyphicon glyphicon-menu-hamburger\"></i> </a> </li> </ul> </div> <button ui-sref=\"login\" class=\"btn btn-success pull-right btn-header-entrar\" ng-if=\"!$root.usuario\"> <i class=\"fa fa-user\"></i> Entrar </button> </nav> </header>"
  );

}]);
