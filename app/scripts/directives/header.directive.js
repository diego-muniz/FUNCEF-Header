﻿(function () {
    'use strict';

    angular
      .module('funcef-header.directive')
      .directive('ngfHeader', ngfHeader);

    /* @ngInject */
    function ngfHeader() {
        return {
            restrict: 'EA',
            replace: true,
            transclude: true,
            templateUrl: 'views/header.view.html',
            controller: 'HeaderController',
            controllerAs: 'vm',
            scope: {
                logo: '@',
                sistema: '@',
                text: '='
            }
        };
    }
})();