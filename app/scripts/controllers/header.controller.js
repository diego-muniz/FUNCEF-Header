﻿(function () {
    'use strict';

    angular.module('funcef-header.controller').
        controller('HeaderController', HeaderController);

    HeaderController.$inject = ['$rootScope', '$cookies', '$state', '$timeout', '$scope'];

    /* @ngInject */
    function HeaderController($rootScope, $cookies, $state, $timeout, $scope) {

        var vm = this;
        vm.toggleMenu = toggleMenu;
        vm.logout = logout;
        vm.trocarUsuario = trocarUsuario;

        /////////////

        /* Fecha e abre menu*/
        function toggleMenu() {
            $rootScope.menuOpen = !$rootScope.menuOpen;
            $cookies.put('menuOpen', $rootScope.menuOpen);
        }

        function logout() {
            $rootScope.usuario = null;

            $timeout(function () {
                $state.go('logout');
            }, 500);

        }

        function trocarUsuario() {
            $rootScope.usuario = null;

            $timeout(function () {
                $state.go('trocar-usuario');
            }, 500);

        }
    }
})();