﻿(function () {
    'use strict';
    /**
    * @ngdoc overview
    * @name Header
    * @version 1.0.0
    * @Componente header para cabeçalho de formulários
    */
    angular.module('funcef-header.controller', []);
    angular.module('funcef-header.directive', []);

    angular
    .module('funcef-header', [
      'funcef-header.controller',
      'funcef-header.directive',
      'ngCookies'
    ]);
})();