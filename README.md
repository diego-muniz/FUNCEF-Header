# Componente - FUNCEF-Header

## Conteúdos

1. [Descrição](#descrição)
2. [Instalação](#instalação)
3. [Script](#script)
4. [CSS](#css)
5. [Módulo](#módulo)
6. [Uso](#uso)
7. [Desinstalação](#desinstalação)

## Descrição

- Componente Header monta o cabeçalho da página possibilitando a inserção de uma imagem (logo) e uma informação de texto (obrigatória para a utilização do componente).
Essa informação pode ser exibida ou não, de acordo com a necessidade.

 
## Instalação:

### Bower

- Necessário executar o bower install e passar o nome do componente.

```
bower install funcef-header --save.
```

## Script

```html
<script src="bower_components/funcef-header/dist/funcef-header.js"></script>
```

__ATENÇÃO:__ Esta linha já é adicionada automaticamente com o uso do grunt pelo wiredep

## CSS  

```html
<link rel="stylesheet" href="bower_components/funcef-header/dist/funcef-header.css">
```

__ATENÇÃO:__ Esta linha já é adicionada automaticamente com o uso do grunt pelo wiredep

## Módulo

- Adicione funfef-header dentro do módulo do Sistema.

```js
angular
        .module('funcef-demo', ['funcef-header']);
```

## Uso

```html
<ngf-header sistema="SISTEMA" logo="/images/logo-funcef.jpg" text="true" ></ngf-header>
```

### Parâmetros adicionais:

- sistema (Obrigatório);
- logo (Opcional);
- text (Opcional);

## Desinstalação:

```
bower uninstall funcef-header --save
```
